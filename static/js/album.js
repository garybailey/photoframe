// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Notifies the backend to load an album into the photo frame queue.
// If the request is successful, the photo frame queue is opened,
// otherwise an error message is shown.
function loadFromAlbum(name, id) {
  showLoadingDialog();
  // Make an ajax request to the backend to load from an album.
  $.ajax({
    type: 'POST',
    url: '/loadFromAlbum',
    dataType: 'json',
    data: {albumId: id},
    success: (data) => {
      $('#albums').empty();

      console.log('Albums imported:' + JSON.stringify(data.parameters));
      if (data.photos && data.photos.length) {
        // Photos were loaded from the album, open the photo frame preview
        // queue.
        window.location = '/';
      } else {
        // No photos were loaded. Display an error.
        handleError('Couldn\'t import album', 'Album is empty.');
      }
      hideLoadingDialog();
    },
    error: (data) => {
      handleError('Couldn\'t import album', data);
    }
  });
}

// Loads a list of all albums owned by the logged in user from the backend.
// The backend returns a list of albums from the Library API that is rendered
// here in a list with a cover image, title and a link to open it in Google
// Photos.
function listAlbums() {
  hideError();
  showLoadingDialog();
  $('#albums').empty();

  $.ajax({
    type: 'GET',
    url: '/getAlbums',
    dataType: 'json',
    success: (data) => {
      console.log('Loaded albums: ' + data.albums);
      // Render each album from the backend in its own row, consisting of
      // title, cover image, number of items, link to Google Photos and a
      // button to add it to the photo frame.
      // The items rendered here are albums that are returned from the
      // Library API.
      $.each(data.albums, (i, item) => {
        // Load the cover photo as a 100x100px thumbnail.
        // It is a base url, so the height and width parameter must be appened.
        const thumbnailUrl = `${item.coverPhotoBaseUrl}=w200-h200`;

        // Setup album card list
        const cardRoot = $('<div />').addClass('card m-3 text-center')
                                      .css('width', '200px');

        // Add card image
        const cardImage = $('<img />')
                          .attr('src', thumbnailUrl)
                          .attr('alt', item.title)
                          .addClass('card-img-top');
        cardRoot.append(cardImage);

        // Create card body
        const cardBody = $('<div />')
                          .addClass('card-body');
        cardRoot.append(cardBody);

        // Create card title
        const cardTitle = $('<h5 />')
                          .addClass('card-title')
                          .text(item.title);
        cardBody.append(cardTitle);

        //Card subtext
        const cardText = $('<p />')
                          .addClass('card-text')
                          .text(`(${item.mediaItemsCount} items)`);
        cardBody.append(cardText);

        //Card action button
        const cardButton = $('<a />')
                            .addClass('btn btn-primary')
                            .addClass('album-title')
                            .attr('data-id', item.id)
                            .attr('data-title', item.title)
                            .attr('href', '#')
                            .text('Select');
        cardBody.append(cardButton);

        // Add the list item to the list of albums.
        $('#albums').append(cardRoot);
      });

      hideLoadingDialog();
      console.log('Albums loaded.');
    },
    error: (data) => {
      hideLoadingDialog();
      handleError('Couldn\'t load albums', data);
    }
  });
}

$(document).ready(() => {
  // Load the list of albums from the backend when the page is ready.
  listAlbums();

  // Clicking the 'add to frame' button starts an import request.
  $('#albums').on('click', '.album-title', (event) => {
    const target = $(event.currentTarget);
    const albumId = target.attr('data-id');
    const albumTitle = target.attr('data-title');

    console.log('Importing album: ' + albumTitle);

    loadFromAlbum(albumTitle, albumId);
  });
});
